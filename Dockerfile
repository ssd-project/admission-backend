FROM python:3.6

EXPOSE 8000

WORKDIR /app

ADD requirements.txt /app
RUN pip3 install -r requirements.txt

ADD . /app

RUN chmod +x run.sh

CMD ./run.sh
