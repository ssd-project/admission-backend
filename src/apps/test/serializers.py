from rest_framework import serializers
from .models import Test, Question, Answer
from rest_framework import serializers


class TestSerializer(serializers.ModelSerializer):
    class Meta:
        model = Test
        fields = ('id', 'name', 'description', 'is_draft')


class QuestionSerializer(serializers.ModelSerializer):
    test_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Question
        fields = ('id', 'test_id', 'text')


class AnswerSerializer(serializers.ModelSerializer):
    question_id = serializers.IntegerField(write_only=True)

    class Meta:
        model = Answer
        fields = ('id', 'question_id', 'text', 'is_correct',)
