from django.core.exceptions import ValidationError
from django.db import models


class Test(models.Model):
    name = models.CharField(
        max_length=64,
        verbose_name='test name'
    )

    description = models.TextField(
        verbose_name='test description'
    )

    is_draft = models.BooleanField(
        default=True,
        verbose_name='is draft'
    )

    def __str__(self):
        return self.name


class Question(models.Model):
    test = models.ForeignKey(
        Test,
        related_name='questions',
        on_delete=models.CASCADE,
    )

    text = models.CharField(
        max_length=512,
        blank=True,
        null=True,
        verbose_name='question text'
    )

    image = models.ImageField(
        blank=True,
        null=True,
        verbose_name='question image'
    )

    def clean(self):
        if not (self.text or self.image):
            raise ValidationError("You must specify either text or image")

    def __str__(self):
        return self.test.name + ': ' + self.text or self.image


class Answer(models.Model):
    question = models.ForeignKey(
        Question,
        related_name='answers',
        on_delete=models.CASCADE
    )

    text = models.CharField(
        max_length=512,
        blank=True,
        null=True,
        verbose_name='answer text'
    )

    image = models.ImageField(
        blank=True,
        null=True,
        verbose_name='answer image'
    )

    is_correct = models.BooleanField(
        default=False,
        verbose_name='is answer correct'
    )

    def clean(self):
        if not (self.text or self.image):
            raise ValidationError("You must specify either text or image")

    def __str__(self):
        return self.text or self.image
