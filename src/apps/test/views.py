from rest_framework import viewsets
from .models import Test, Question, Answer
from .serializers import TestSerializer, QuestionSerializer, AnswerSerializer
from rest_framework import generics, mixins
from rest_framework.viewsets import GenericViewSet
from rest_framework.response import Response
from ..admission.permissions import ManagerPermission
from rest_framework.views import APIView


class TestViewSet(mixins.CreateModelMixin,
                  mixins.ListModelMixin,
                  mixins.RetrieveModelMixin,
                  mixins.DestroyModelMixin,
                  mixins.UpdateModelMixin,
                  GenericViewSet):
    queryset = Test.objects.all()
    serializer_class = TestSerializer
    permission_classes = (ManagerPermission,)


class QuestionViewSet(mixins.CreateModelMixin,
                      mixins.ListModelMixin,
                      mixins.RetrieveModelMixin,
                      mixins.DestroyModelMixin,
                      mixins.UpdateModelMixin,
                      GenericViewSet):
    queryset = Question.objects.all()
    serializer_class = QuestionSerializer
    permission_classes = (ManagerPermission,)


class AnswerViewSet(mixins.CreateModelMixin,
                    mixins.ListModelMixin,
                    mixins.RetrieveModelMixin,
                    mixins.DestroyModelMixin,
                    mixins.UpdateModelMixin,
                    GenericViewSet):
    queryset = Answer.objects.all()
    serializer_class = AnswerSerializer
    permission_classes = (ManagerPermission,)


class QuestionAnswersView(APIView):
    permission_classes = (ManagerPermission,)

    def post(self, request, format=None):
        data = request.data
        question_id = data["question_id"]
        response = Answer.objects.filter(question_id=question_id)
        Response(response)
