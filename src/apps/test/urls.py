from django.urls import include, path
from django.conf.urls import url
from rest_framework import routers

from . import views


router = routers.DefaultRouter()
router.register('questions', views.QuestionViewSet)
router.register('answers', views.AnswerViewSet)
router.register('', views.TestViewSet)


urlpatterns = [
    url(r'^', include(router.urls)),
    path("qustions/answers/", views.QuestionAnswersView.as_view())
]
