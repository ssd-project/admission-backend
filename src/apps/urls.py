from django.contrib import admin
from django.urls import path, include
from rest_framework import routers
from .admission import views

urlpatterns = [
    path('tests/', include('src.apps.test.urls')),
    path('', include('src.apps.admission.urls')),
]
