from django.contrib import admin

from .models import EnrollmentTest, EduProgram, EnrolleeAnswer, EnrolleeTest, EnrolleeDocument, Enrollee
from .forms import CustomUserCreationForm, CustomUserChangeForm


@admin.register(Enrollee)
class EnrolleeAdmin(admin.ModelAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = Enrollee
    list_display = ['email']


@admin.register(EnrolleeDocument)
class EnrolleeDocumentAdmin(admin.ModelAdmin):
    pass


@admin.register(EnrolleeTest)
class EnrolleeTestAdmin(admin.ModelAdmin):
    pass


@admin.register(EnrolleeAnswer)
class EnrolleeAnswerAdmin(admin.ModelAdmin):
    pass


@admin.register(EduProgram)
class EduProgramAdmin(admin.ModelAdmin):
    pass


@admin.register(EnrollmentTest)
class EnrollmentTestAdmin(admin.ModelAdmin):
    pass
