from django.contrib.auth import password_validation


class PasswordFieldsValidator(object):
    def __call__(self, data):
        for key in data:
            value = str(data[key])
            if 'password' in key:
                password_validation.validate_password(value)


