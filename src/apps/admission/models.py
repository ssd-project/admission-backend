from django.contrib.auth.models import AbstractUser
from django.db import models
from django.conf import settings
from django.contrib.auth.base_user import BaseUserManager
from src.apps.test.models import Answer, Test, Question
from django.utils import timezone
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
import uuid
import os


class UserManager(BaseUserManager):

    def _create_user(self, email, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given email must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff,
                          is_superuser=is_superuser, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, password=None, **extra_fields):
        return self._create_user(email, password, False, False,
                                 **extra_fields)

    def create_staff(self, email, password=None, **extra_fields):
        return self._create_user(email, password, True, False,
                                 **extra_fields)

    def create_superuser(self, email, password, **extra_fields):
        return self._create_user(email, password, False, True,
                                 **extra_fields)

    def get_by_natural_key(self, username):
        case_insensitive_username_field = '{}__iexact'.format(self.model.USERNAME_FIELD)
        return self.get(**{case_insensitive_username_field: username})


class Enrollee(AbstractBaseUser, PermissionsMixin):
    email = models.EmailField('email', max_length=255, unique=True)
    first_name = models.CharField('first_name', max_length=255)
    last_name = models.CharField('last_name', max_length=255)
    birth_date = models.DateField('birth_date', auto_now=False, null=True)
    country = models.CharField(blank=True, null=True, max_length=30)
    city = models.CharField(blank=True, null=True, max_length=30)
    edu_program = models.ForeignKey(
        'EduProgram',
        default=None,
        related_name='enrollies',
        on_delete=models.CASCADE,
        null=True
    )

    is_staff = models.BooleanField('staff status', default=False)
    is_manager = models.BooleanField('manager status', default=False)

    objects = UserManager()

    REQUIRED_FIELDS = []

    USERNAME_FIELD = 'email'

    def __str__(self):
        return self.email


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s.%s" % (uuid.uuid4(), ext)
    return os.path.join(filename)


class Interview(models.Model):
    enrollee = models.OneToOneField(
        Enrollee,
        on_delete=models.CASCADE,
        related_name='interview',
        editable=False,
    )

    staff = models.ForeignKey(
        Enrollee,
        on_delete=models.CASCADE,
        related_name='interviews',
        editable=False,
    )

    score = models.PositiveIntegerField(null=True)

    feedback = models.TextField(null=True)

    passed = models.BooleanField(default=False)

    def __str__(self):
        return self.enrollee.email + " " + self.staff.email


class EnrolleeDocument(models.Model):
    CV = 1
    DIPLOMA = 2
    MOTIVATION_LETTER = 3
    RECOMMENDATION = 4
    ID_SCAN = 5

    DOC_TYPES = (
        (CV, 'CV'),
        (DIPLOMA, 'Diploma'),
        (MOTIVATION_LETTER, 'Motivation letter'),
        (RECOMMENDATION, 'Recommendation letter'),
        (ID_SCAN, 'ID scan')
    )

    enrollee = models.ForeignKey(
        Enrollee,
        related_name='documents',
        on_delete=models.CASCADE
    )

    doc_type = models.PositiveIntegerField(
        choices=DOC_TYPES
    )

    file = models.FileField(null=True, blank=True, upload_to=get_file_path)

    def __str__(self):
        return (self.enrollee.email + ' ' + str(self.doc_type))


class EnrolleeTest(models.Model):
    enrollee = models.ForeignKey(
        Enrollee,
        related_name='tests',
        on_delete=models.CASCADE
    )

    test = models.ForeignKey(
        'EnrollmentTest',
        related_name='enrollee_tests',
        on_delete=models.CASCADE
    )

    is_complete = models.BooleanField(
        default=False
    )

    result = models.DecimalField(
        max_digits=4,
        decimal_places=2,
        default=0
    )

    def __str__(self):
        return (self.enrollee.user.get_full_name() or self.enrollee.user.get_username()) + ': ' + str(self.test)


class EnrolleeAnswer(models.Model):
    test = models.ForeignKey(
        EnrolleeTest,
        related_name='enrollee_answers',
        on_delete=models.CASCADE
    )

    question = models.ForeignKey(
        Question,
        related_name='question',
        on_delete=models.CASCADE
    )

    text = models.CharField(
        max_length=512,
        blank=True,
        null=True,
        verbose_name='answer text'
    )

    is_correct = models.BooleanField(
        default=False,
        verbose_name='is answer correct'
    )

    def __str__(self):
        return (self.test.enrollee.user.get_full_name() or self.test.enrollee.user.get_username()) + ': ' + str(
            self.answer)


class EduProgram(models.Model):
    name = models.CharField(
        max_length=64
    )

    def __str__(self):
        return self.name


class EnrollmentTest(models.Model):
    TRAIN_TEST = 1
    OBLIGATORY_TEST = 2

    TEST_TYPES = (
        (TRAIN_TEST, 'Train test'),
        (OBLIGATORY_TEST, 'Obligatory test')
    )

    test = models.ForeignKey(
        Test,
        related_name='enrollment_tests',
        on_delete=models.CASCADE
    )

    edu_program = models.ForeignKey(
        EduProgram,
        related_name='enrollment_tests',
        on_delete=models.CASCADE
    )

    test_type = models.PositiveSmallIntegerField(
        choices=TEST_TYPES
    )

    def __str__(self):
        return self.edu_program.name + ': ' + self.test.name
