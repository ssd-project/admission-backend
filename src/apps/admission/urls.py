from django.urls import include, path, re_path
from django.conf.urls import url
from rest_framework import routers
from rest_framework_jwt.views import obtain_jwt_token, verify_jwt_token

from .views import RegisterEnrolleeView, EnrolleeViewSet, EnrolleeDocumentViewSet, EnrolleeProfileView, \
    RegisterStaffView, StaffViewSet, InterviewViewSet, RoleView, EduProgramViewSet, EnrollmentTestViewSet, \
    EnrolleeTestView, EnrolleeTestCheckView

router = routers.DefaultRouter()
router.register(r'documents', EnrolleeDocumentViewSet, basename='documents')
router.register(r'staff', StaffViewSet, basename='staff')
router.register(r'interviews', InterviewViewSet, basename='interview')
router.register('', EnrolleeViewSet)

eduRouter = routers.DefaultRouter()
eduRouter.register('enrollments', EnrollmentTestViewSet)
# eduRouter.register('enrollee-test/answer', AnswerViewSet)
eduRouter.register('enrollee-test', EnrolleeTestView)
eduRouter.register('', EduProgramViewSet)
urlpatterns = [
    url(r'^users/auth/$', obtain_jwt_token),
    url(r'^users/api-token-verify/', verify_jwt_token),
    path('users/registration/', RegisterEnrolleeView.as_view()),
    path('users/role/', RoleView.as_view()),
    path('users/profile/', EnrolleeProfileView.as_view()),
    path('users/staff/registration/', RegisterStaffView.as_view()),
    path('users/', include(router.urls)),
    path('edu/enrollee-test/check', EnrolleeTestCheckView.as_view()),
    path('edu/', include(eduRouter.urls)),
]
