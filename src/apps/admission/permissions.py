from rest_framework.permissions import BasePermission


class StaffPermission(BasePermission):

    def has_permission(self, request, view):
        return not request.user.is_anonymous and (request.user.is_staff or request.user.is_manager or request.user.is_superuser)

    def has_object_permission(self, request, view, obj):
        return not request.user.is_anonymous and (request.user.is_staff or request.user.is_manager or request.user.is_superuser)


class ManagerPermission(BasePermission):

    def has_permission(self, request, view):
        return not request.user.is_anonymous and (request.user.is_manager or request.user.is_superuser or request.user.is_manager)

    def has_object_permission(self, request, view, obj):
        return not request.user.is_anonymous and (request.user.is_manager or request.user.is_superuser or request.user.is_manager)
