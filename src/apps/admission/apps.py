from django.apps import AppConfig


class AdmissionConfig(AppConfig):
    name = 'src.apps.admission'
