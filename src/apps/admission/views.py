from rest_framework import viewsets, mixins, generics, permissions, status
from .models import Enrollee, EduProgram, EnrollmentTest, EnrolleeDocument, Interview, EnrolleeTest
from .serializers import EnrolleeSerializer, EduProgramSerializer, EnrollmentTestSerializer, EnrolleeRegisterSerializer, \
    EnrolleeDocumentSerializer, StaffRegisterSerializer, StaffSerializer, InterviewSerializer, \
    EnrolleeInterviewSerializer, StaffInterviewSerializer

from rest_framework.viewsets import GenericViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from .permissions import ManagerPermission
from rest_framework_jwt.authentication import JSONWebTokenAuthentication


class EnrolleeViewSet(mixins.ListModelMixin, mixins.RetrieveModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    queryset = Enrollee.objects.filter(is_staff=False, is_manager=False, is_superuser=False)
    serializer_class = EnrolleeSerializer

    def update(self, request, *args, **kwargs):
        enrollee = self.get_object()
        serializer = self.get_serializer(enrollee, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return mixins.Response(serializer.data)


class EnrolleeProfileView(APIView):
    authentication_classes = (JSONWebTokenAuthentication,)

    def get(self, request, format=None):
        if request.user.is_anonymous:
            return Response(data={'response': "пшел отсюда, незареганный, фу"}, status=status.HTTP_401_UNAUTHORIZED)
        serializer = EnrolleeSerializer(request.user)
        return Response(serializer.data)


class EduProgramViewSet(viewsets.ModelViewSet):
    queryset = EduProgram.objects.all()
    serializer_class = EduProgramSerializer


class EnrollmentTestViewSet(viewsets.ModelViewSet):
    queryset = EnrollmentTest.objects.all()
    serializer_class = EnrollmentTestSerializer


class EnrollmentTestListView(generics.ListCreateAPIView):
    queryset = EnrollmentTest.objects.all()
    serializer_class = EnrollmentTestSerializer


class RegisterEnrolleeView(generics.CreateAPIView):
    serializer_class = EnrolleeRegisterSerializer
    model = Enrollee


class RegisterStaffView(generics.CreateAPIView):
    permission_classes = (ManagerPermission,)
    serializer_class = StaffRegisterSerializer
    model = Enrollee


class StaffViewSet(mixins.ListModelMixin, mixins.UpdateModelMixin, GenericViewSet):
    queryset = Enrollee.objects.filter(is_staff=True, is_superuser=False)
    permissions = (ManagerPermission,)
    serializer_class = StaffSerializer
    model = Enrollee

    def update(self, request, *args, **kwargs):
        enrollee = self.get_object()
        serializer = self.get_serializer(enrollee, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return mixins.Response(serializer.data)


class EnrolleeDocumentViewSet(mixins.CreateModelMixin,
                              mixins.ListModelMixin,
                              GenericViewSet, ):
    authentication_classes = (JSONWebTokenAuthentication,)
    queryset = EnrolleeDocument.objects.all()
    serializer_class = EnrolleeDocumentSerializer
    model = EnrolleeDocument

    def get_queryset(self):
        user = self.request.user
        return self.model.objects.filter(enrollee_id=user.id)
    # def destroy(self, request, *args, **kwargs):
    #     instance = self.get_object()
    #     if request.user == instance.user:
    #         self.perform_destroy(instance)
    #         return Response(status=status.HTTP_204_NO_CONTENT)
    #     else:
    #         return Response(status=status.HTTP_403_FORBIDDEN)

    # def create(self, request, *args, **kwargs):
    #     user = request.user
    #     doc_type = request.data["doc_type"]
    #     # print(user, doc_type)
    #     # print(user.id)
    #     document = EnrolleeDocument.objects.create(doc_type=doc_type, enrollee_id=user.id)
    #     content = {
    #         'user': str(request.user),  # `django.contrib.auth.User` instance.
    #         'document_id': document.id,  # None
    #     }
    #     return Response(content, status=status.HTTP_201_CREATED)


class InterviewViewSet(mixins.CreateModelMixin,
                       mixins.ListModelMixin,
                       mixins.DestroyModelMixin,
                       mixins.UpdateModelMixin,
                       GenericViewSet):
    queryset = Interview.objects.all()
    # serializer_class = InterviewSerializer
    model = Interview
    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        print(self.request.user.is_superuser)
        print(self.request.user.is_staff)
        print(self.request.user.is_manager)
        if self.request.user.is_superuser or self.request.user.is_manager:
            return Interview.objects.all()
        elif self.request.user.is_staff:
            return Interview.objects.filter(staff_id=self.request.user.id)
        elif not (self.request.user.is_staff or self.request.user.is_superuser or self.request.user.is_manager):
            return Interview.objects.filter(enrollee_id=self.request.user.id)
        return Interview.objects.none()

    def get_serializer_class(self):
        if self.request.user.is_staff and not self.request.user.is_superuser:
            return StaffInterviewSerializer
        if not (self.request.user.is_staff or self.request.user.is_superuser or self.request.user.is_manager):
            return EnrolleeInterviewSerializer
        return InterviewSerializer

    def update(self, request, *args, **kwargs):
        if not self.request.user.is_staff:
            return Response(status=status.HTTP_403_FORBIDDEN)
        instance = self.get_object()
        if self.request.user.id != instance.staff_id:
            return Response(status=status.HTTP_403_FORBIDDEN)
        data = request.data
        if 'enrollee_id' in data:
            data.pop('enrollee_id')
        if 'staff_id' in data:
            data.pop('staff_id')
        data['passed'] = True
        serializer = self.get_serializer(instance, data=data)  # test this
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)
        return Response(serializer.data)


class RoleView(APIView):
    def get(self, request, format=None):
        if self.request.user.is_anonymous:
            response = {
                'enrollee': False,
                'staff': False,
                'superuser': False,
                'manager': False,
                'anonymous': True,
            }
        else:
            response = {
                'enrollee': not (self.request.user.is_staff
                                 or self.request.user.is_superuser
                                 or self.request.user.is_manager
                                 or self.request.user.is_anonymous),
                'staff': self.request.user.is_staff,
                'superuser': self.request.user.is_superuser,
                'manager': self.request.user.is_manager,
                'anonymous': self.request.user.is_anonymous,
            }
        return Response(response)


class EnrollmentTestView(mixins.CreateModelMixin,
                         mixins.UpdateModelMixin,
                         mixins.ListModelMixin,
                         mixins.DestroyModelMixin,
                         mixins.RetrieveModelMixin,
                         GenericViewSet):
    queryset = EnrollmentTest.objects.all()
    model = EnrollmentTest
    permission_classes = (ManagerPermission,)


class EnrolleeTestView(viewsets.ModelViewSet):
    queryset = EnrollmentTest.objects.all()
    model = EnrolleeTest
    serializer_class = EnrollmentTestSerializer
    permission_classes = (permissions.IsAuthenticated,)


class EnrolleeAnswerView(viewsets.ModelViewSet):
    queryset = EnrollmentTest.objects.all()
    model = EnrolleeTest
    serializer_class = EnrollmentTestSerializer
    permission_classes = (permissions.IsAuthenticated,)


class EnrolleeTestCheckView(APIView):
    permission_classes = (permissions.IsAuthenticated,)

    def post(self, request, format=None):
        try:
            enrollee_test_id = request.data.test_id
        except:
            return Response(status=status.HTTP_400_BAD_REQUEST)
        test = EnrolleeTest.objects.get(id=enrollee_test_id)
        enrollee_answers = test.enrollee_answers
        test_answers = test.enrollee_tests.enrollment_tests.answers.objects.filter(is_correct=True)
