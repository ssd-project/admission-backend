from rest_framework import serializers

from .models import Enrollee, EduProgram, EnrollmentTest, EnrolleeDocument, Interview, EnrolleeTest, EnrollmentTest, \
    EnrolleeAnswer

from rest_framework.validators import UniqueValidator
from .validators import PasswordFieldsValidator
from django.contrib.auth import authenticate


class EduProgramSerializer(serializers.ModelSerializer):
    class Meta:
        model = EduProgram
        fields = ('id', 'name',)

    # Абстрактаня функция для добавления теста в курс
    def add_test_for_course(self, context):
        """
        :param context: должен содержать айдишники курса, теста, и определённый тип теста
        """

        enroll_test = EnrollmentTest(
            edu_program=context.edu_progra_id,
            test=context.test_id,
            test_type=context.test_type
        )
        enroll_test.save()

    # Абстрактаня функция для редактирования проходного балла теста в курс
    def edit_test__pass_score(self, context):
        """
        :param context: должен содержать айдишник курсового теста
            и проходной балл
        """
        try:
            test_objj = EnrollmentTest.objects.get(
                id=context.test_id)
        except EnrollmentTest.DoesNotExist:
            return

        test_objj.pass_core = context.pass_core
        test_objj.save()


class EnrolleeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enrollee
        fields = ('id', 'email', 'first_name', 'last_name', 'birth_date', 'country', 'city', 'edu_program')

    def get_available_tests(self):
        ''' Раскоментить после нормальной стуктуризации базы
        test_ids = EnrollmentTest.objects.filter(
            edu_program=self.edu_program
        ).values_list('test', flat=True)
        '''

        result_query = EnrolleeTest.objects.filter(
            enrollee=self.id,
            # test__in=test_ids
        ).values(
            'enrollee__edu_program__name',
            'test__name',
            'result'
        )

        return result_query
    # def update(self, instance, validated_data):
    #     instance.email = validated_data.get('email', instance.email)
    #     instance.save()
    #     return instance

    # def restore_object(self, attrs, instance=None):
    #     """
    #     Given a dictionary of deserialized field values, either update
    #     an existing model instance, or create a new model instance.
    #     """
    #     if instance is not None:
    #         instance.user.email = attrs.get('user.email', instance.user.email)
    #         instance.ban_status = attrs.get('ban_status', instance.ban_status)
    #         instance.user.password = attrs.get('user.password', instance.user.password)
    #         return instance
    #
    #     user = User.objects.create_user(username=attrs.get('user.username'), email=attrs.get('user.email'),
    #                                     password=attrs.get('user.password'))
    #     return Enrollee(user=user)


class EnrolleeRegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=Enrollee.objects.all())]
    )
    first_name = serializers.CharField(write_only=True, required=True)
    last_name = serializers.CharField(write_only=True, required=True)
    birth_date = serializers.DateField(write_only=True, required=True)
    country = serializers.CharField(write_only=True, required=True)
    city = serializers.CharField(write_only=True, required=True)
    password = serializers.CharField(write_only=True)
    edu_program_id = serializers.IntegerField(required=True)

    class Meta:
        validators = [PasswordFieldsValidator(), ]
        # fields = ('email', 'first_name', "second_name", 'birth_date', 'country', 'city', 'password', 'edu_program',)

    def validate_edu_program(self, edu_program_id):
        if not EduProgram.objects.filter(id=edu_program_id).exists():
            raise serializers.ValidationError('Edu program bot exists')
        return edu_program_id

    def create(self, validated_data):
        email = validated_data['email']
        password = validated_data['password']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        birth_date = validated_data['birth_date']
        country = validated_data['country']
        city = validated_data['city']
        edu_program_id = validated_data['edu_program_id']
        # edu_program = EduProgram.objects.get(id=validated_data['edu_program'])
        user = Enrollee.objects.create_user(
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
            birth_date=birth_date,
            country=country,
            city=city,
            edu_program_id=edu_program_id
        )
        return user


class StaffSerializer(serializers.ModelSerializer):
    class Meta:
        model = Enrollee
        fields = ('id', 'email', 'first_name', 'last_name',)


class StaffRegisterSerializer(serializers.Serializer):
    email = serializers.EmailField(
        validators=[UniqueValidator(queryset=Enrollee.objects.all())]
    )
    first_name = serializers.CharField(required=True)
    last_name = serializers.CharField(required=True)
    password = serializers.CharField(write_only=True, required=True)

    class Meta:
        validators = [PasswordFieldsValidator(), ]
        # fields = ('id', 'email', 'first_name', "second_name",)

    def create(self, validated_data):
        email = validated_data['email']
        password = validated_data['password']
        first_name = validated_data['first_name']
        last_name = validated_data['last_name']
        # edu_program = EduProgram.objects.get(id=validated_data['edu_program'])
        user = Enrollee.objects.create_staff(
            email=email,
            password=password,
            first_name=first_name,
            last_name=last_name,
        )
        return user


class EnrolleeDocumentSerializer(serializers.ModelSerializer):
    doc_type = serializers.IntegerField(required=True)
    file = serializers.FileField(max_length=None, allow_empty_file=False, use_url=False)
    enrollee = serializers.HiddenField(
        default=serializers.CurrentUserDefault()
    )

    class Meta:
        model = EnrolleeDocument
        fields = ('id', 'doc_type', 'file', 'enrollee')

    def validate_doc_type(self, value):
        if value < 1 or value > 5:
            raise serializers.ValidationError("Doc type not exists")
        return value


class EnrolleeInterviewSerializer(serializers.ModelSerializer):
    class Meta:
        model = Interview
        fields = ('enrollee_id', 'staff_id', 'passed')


class InterviewSerializer(serializers.ModelSerializer):
    staff_id = serializers.IntegerField()
    enrollee_id = serializers.IntegerField()

    class Meta:
        model = Interview
        fields = ('id', 'staff_id', 'enrollee_id', 'feedback', 'score', 'passed')

    def validate_enrollee_id(self, value):
        enrollee = Enrollee.objects.get(pk=value)
        if not (enrollee.is_manager or enrollee.is_staff):
            if enrollee.interview is not None:
                raise serializers.ValidationError("Enrollee already has assigned interview")
            return value
        else:
            raise serializers.ValidationError("User not enrollee")

    def validate_staff_id(self, value):
        enrollee = Enrollee.objects.get(pk=value)
        if enrollee.is_staff:
            return value
        else:
            raise serializers.ValidationError("User not staff")

    def create(self, validated_data):
        enrollee_id = validated_data['enrollee_id']
        staff_id = validated_data['staff_id']

        interview = Interview.objects.create(
            enrollee_id=enrollee_id,
            staff_id=staff_id
        )
        return interview


class StaffInterviewSerializer(serializers.ModelSerializer):
    feedback = serializers.CharField(required=True)
    score = serializers.IntegerField(required=True)

    def validate_score(self, value):
        if value >= 0 and value <= 100:
            return value
        else:
            raise serializers.ValidationError("Value is not correct. Should be 0..100")

    class Meta:
        model = Interview
        fields = ('id', 'staff_id', 'enrollee_id', 'feedback', 'score', 'passed')


class EnrollmentTestSerializer(serializers.ModelSerializer):
    test_id = serializers.IntegerField(required=True)
    edu_program_id = serializers.IntegerField(required=True)
    test_type = serializers.IntegerField(required=True)

    class Meta:
        model = EnrollmentTest
        fields = ('id', 'test_type', 'test_id', 'edu_program_id',)


class EnrolleeTestSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnrolleeTest
        fields = ('id', 'test_id', 'is_complete', 'result')


class EnrolleeAnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = EnrolleeAnswer
        fields = ('id', 'test_id', 'question_id', 'text')