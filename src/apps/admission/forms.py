from django.contrib.auth.forms import UserCreationForm, UserChangeForm
from .models import Enrollee


class CustomUserCreationForm(UserCreationForm):
    class Meta(UserCreationForm):
        model = Enrollee
        fields = ('email',)


class CustomUserChangeForm(UserChangeForm):
    class Meta:
        model = Enrollee
        fields = UserChangeForm.Meta.fields
