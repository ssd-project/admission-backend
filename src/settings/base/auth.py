AUTH_USER_MODEL = 'admission.Enrollee'
ACCOUNT_EMAIL_VERIFICATION = False
ACCOUNT_LOGOUT_ON_GET = False
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_USERNAME_REQUIRED = False

ACCOUNT_AUTHENTICATION_METHOD = 'email'
USER_MODEL_USERNAME_FIELD = 'email'
SITE_ID = 1
# ACCOUNT_ADAPTER = 'src.apps.admission.adapters.EnrolleeAdapter'


# email settings
EMAIL_HOST = 'smtp.inno-admission.live'
EMAIL_HOST_USER = 'test@inno-admission.live'
EMAIL_HOST_PASSWORD = 'SecuredPassword'
EMAIL_PORT = 587
EMAIL_USE_TLS = True
DEFAULT_FROM_EMAIL = EMAIL_HOST_USER

# REST_AUTH_REGISTER_SERIALIZERS = {
#     'REGISTER_SERIALIZER': 'src.apps.admission.serializers.EnrolleeRegisterSerializer',
# }
