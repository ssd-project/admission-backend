# SECURITY WARNING: keep the secret key used in production secret!
import os

SECRET_KEY = 'cp@6rm*jwhhmq!ai4#3#drfbad@jzk7*0g06+8xs^8&pg07y&5'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']
CORS_ORIGIN_ALLOW_ALL = True

CORS_ORIGIN_WHITELIST = (
    'localhost:8000',
    '127.0.0.1:9000',
    'inno-admission.live',
    'inno-admission.live:8000',
    'localhost:3000',
    'http://localhost:3000',
    'http://127.0.0.1:3000',
    '127.0.0.1:3000',
    '0.0.0.0:3000',
    'http://0.0.0.0:3000',
)
# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

# AUTH_PASSWORD_VALIDATORS = [
#     {
#         'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
#     },
#     {
#         'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
#     },
# ]

SITE_ID = 1
