INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',

    'rest_framework_swagger',
    'rest_framework',
    'rest_framework.authtoken',

    'src.apps.test',
    'src.apps.admission',
    'rest_auth',
    'corsheaders',
]
